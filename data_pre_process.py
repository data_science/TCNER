import os

from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer, PorterStemmer

PATH_TO_DIR = os.path.join(".", "data", "old")


def lemmatize_raw_lines(raw_lines):
    lemmatizer = WordNetLemmatizer()
    for i, line in enumerate(raw_lines):
        line = line.split()
        temp = [lemmatizer.lemmatize(word) for word in line]
        if temp != line:
            print(f"{line} -> {temp}")
        raw_lines[i] = temp
    return raw_lines


def stem_raw_lines(raw_lines):
    stemmer = PorterStemmer()
    for i, line in enumerate(raw_lines):
        line = line.split()
        temp = [stemmer.stem(word) for word in line]
        if temp != line:
            print(f"{line} -> {temp}")
        raw_lines[i] = temp
    return raw_lines


def lemmatize_ds(dataset):
    tweets = dataset["tweet"]
    lemmatizer = WordNetLemmatizer()
    for i, tweet in enumerate(tweets):
        temp = [lemmatizer.lemmatize(word) for word in tweet]
        if temp != tweet:
            print(f"{tweet} -> {temp}")
            if len(temp) != len(tweet):
                raise ValueError(f"Lengths differ: {len(temp)} != {len(tweet)}")
        tweets[i] = temp
    dataset["tweet"] = tweets
    return dataset


def stem_ds(dataset):
    tweets = dataset["tweet"]
    stemmer = PorterStemmer()
    for i, tweet in enumerate(tweets):
        temp = [stemmer.stem(word) for word in tweet]
        if temp != tweet:
            print(f"{tweet} -> {temp}")
            if len(temp) != len(tweet):
                raise ValueError(f"Lengths differ: {len(temp)} != {len(tweet)}")
        tweets[i] = temp
    dataset["tweet"] = tweets
    return dataset


def lower_case_ds(dataset):
    tweets = dataset["tweet"]
    for i, tweet in enumerate(tweets):
        tweets[i] = [word.lower() for word in tweet]
    dataset["tweet"] = tweets
    return dataset


def filter_stop_words_ds(dataset):
    tweets, ner_tags = dataset["tweet"], dataset["ner_tags"]
    eng_stop_words = set(stopwords.words("english"))

    # if a stop word is removed, the corresponding ner tag must be removed as well
    # therefore, the indices of the stop words are stored in a list
    # and the same indices are removed from the ner tags
    for i, tweet in enumerate(tweets):
        tweet: list
        indices = []
        for j, word in enumerate(tweet):
            if word in eng_stop_words or word.lower() == "rt" or word == "HTTPURL" or word == "@USER":
                indices.append(j)
                if ner_tags[i][j] != 0:
                    raise ValueError(f"Stop word {word} has a NER tag: {ner_tags[i][j]}")
        if indices:
            for j in sorted(indices, reverse=True):
                tweet.pop(j)
                ner_tags[i].pop(j)


if __name__ == '__main__':
    from datasets import DatasetDict, Dataset
    from datasets import load_from_disk, Sequence, Value, ClassLabel, Features

    # for mode in ['IOB', 'IOBES']:
    #     ds = load_from_disk(os.path.join(PATH_TO_DIR, f"train_test_{mode}_lemmatized"))
    #     ds = ds.map(lower_case_ds, batched=True)
    #     ds.save_to_disk(os.path.join(PATH_TO_DIR, f"train_test_{mode}_lemmatized_uncased"))

    # there are 5 different preprocessing methods: none, lemmatization, stemming, lower casing and lower casing + lemmatization
    # and there 2 different NER tagging schemes: IOB and IOBES
    # therefore, there are 10 different datasets
    # read these datasets and create one dataset over all

    iob_list = ['O', 'B-ORG', 'I-ORG', 'B-LOC', 'I-LOC', 'B-PER', 'I-PER', 'B-MISC', 'I-MISC']

    iobes_list = [
        'O',
        'B-ORG', 'I-ORG', 'E-ORG', 'S-ORG', 'B-LOC', 'I-LOC', 'E-LOC', 'S-LOC',
        'B-PER', 'I-PER', 'E-PER', 'S-PER', 'B-MISC', 'I-MISC', 'E-MISC', 'S-MISC',
    ]


    all_datasets = {}
    for prep in ['none', 'lemmatized', 'stemmed', 'uncased', 'lemmatized_uncased']:
        all_datasets[prep] = {}
        for mode in ['IOB', 'IOBES']:
            all_datasets[prep][mode] = load_from_disk(os.path.join(PATH_TO_DIR, f"train_test_{mode}_{prep}"))

    # merge all datasets
    train_inputs = {
        f'tokens_{prep}': all_datasets[prep]['IOB']['train']['tweet'] for prep in all_datasets
    }
    train_labels = {
        f'ner_tags_{mode}': all_datasets['none'][mode]['train']['ner_tags'] for mode in all_datasets['none']
    }

    test_inputs = {
        f'tokens_{prep}': all_datasets[prep]['IOB']['test']['tweet'] for prep in all_datasets
    }
    test_labels = {
        f'ner_tags_{mode}': all_datasets['none'][mode]['test']['ner_tags'] for mode in all_datasets['none']
    }

    features = Features(
        {
            **{f'tokens_{prep}': Sequence(Value(dtype='string')) for prep in all_datasets},
            'ner_tags_IOB': Sequence(ClassLabel(num_classes=9, names=iob_list, id=None)),
            'ner_tags_IOBES': Sequence(ClassLabel(num_classes=17, names=iobes_list, id=None)),
        }
    )

    train_dataset = Dataset.from_dict({**train_inputs, **train_labels}, features=features)
    test_dataset = Dataset.from_dict({**test_inputs, **test_labels}, features=features)

    ds = DatasetDict({'train': train_dataset, 'test': test_dataset})
    ds.save_to_disk(os.path.join(PATH_TO_DIR, "train_test_all"))

    # load the dataset
    ds = load_from_disk(os.path.join(PATH_TO_DIR, "train_test_all"))
    ds.push_to_hub("tweet_ner_dataset", private=True)
