# read the tweets from the file TweetsTrainset.txt
# and write them to the file raw_tweets.txt
def dump_tweets():
    with open("TweetsTrainset.txt", "r") as f:
        tweets = f.readlines()
    with open("raw_tweets.txt", "w") as f:
        for tweet in tweets:
            f.write(tweet.split("\t")[2])


if __name__ == '__main__':
    dump_tweets()
