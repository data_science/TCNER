# find the distribution of the tags in the dataset

import os
import pandas as pd

import seaborn as sns
import matplotlib.pyplot as plt

# set the path to the dataset
path_train = os.path.join('.', 'TweetsTrainset.txt')
path_test = os.path.join('.', 'TweetsTestGroundTruth.txt')


def read_file(path, encoding='utf-8'):
    with open(path, 'r', encoding=encoding) as f:
        lines = f.read().splitlines()
    return lines


def get_tags(lines):
    all_tags = pd.Series(dtype='str')
    for line in lines:
        line = line.split('\t')
        tags = pd.Series(map(lambda ne: ne.split('/')[0].strip().upper(), line[1].split(';')))
        tags = tags[tags != '']
        if len(tags) > 0:
            all_tags = pd.concat([all_tags, tags])
        else:
            all_tags = pd.concat([all_tags, pd.Series(['None'])])
    return all_tags


def plot_tags_distribution(tags, title):
    sns.set(style="darkgrid")
    ax = sns.countplot(x=tags, order=tags.value_counts().index)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=40, ha="right")
    ax.set_ylabel('Number of Occurrences')
    ax.set_xlabel('Tags')
    plt.tight_layout()
    plt.savefig(f'tags_distribution_{title}.pdf')
    plt.show()
    return tags.value_counts()


if __name__ == '__main__':
    # read the data
    lines_train = read_file(path_train)
    lines_test = read_file(path_test)

    s1 = plot_tags_distribution(get_tags(lines_train), 'train')
    s2 = plot_tags_distribution(get_tags(lines_test), 'test')

    # make a table with the distribution of the tags
    df = pd.DataFrame({
        'train': s1,
        'test': s2
    })

    print(df.to_latex())
