import json

import datasets
import pandas as pd
from tqdm.auto import tqdm

import torch
import numpy as np

import evaluate
from datasets import load_from_disk
from transformers import TrainingArguments, Trainer, PrinterCallback
from transformers import DataCollatorForTokenClassification
from transformers import AutoTokenizer, AutoModelForTokenClassification
from transformers import TrainerCallback, TrainerState, TrainerControl, ProgressCallback

BASE_MODEL = "cardiffnlp/twitter-roberta-base"

PATH_TO_DIR = "."


datasets.disable_progress_bar()


class TQDMCallback(TrainerCallback):
    def __init__(self):
        self.progress_bar: tqdm = None
        self.loss = float("inf")

    def on_train_begin(self, args: TrainingArguments, state: TrainerState, control: TrainerControl, **kwargs):
        self.progress_bar = tqdm(
            total=state.max_steps, desc=f"Training...",
            position=0, leave=True
        )
        self.progress_bar.set_postfix(step=f'0/{state.max_steps}', loss=self.loss)

    def on_step_end(self, args: TrainingArguments, state: TrainerState, control: TrainerControl, **kwargs):
        self.progress_bar.set_postfix(loss=self.loss, step=f'{state.global_step}/{state.max_steps}')
        self.progress_bar.update(1)

    def on_log(self, args: TrainingArguments, state: TrainerState, control: TrainerControl, **kwargs):
        if "loss" in (log := state.log_history[-1]):
            self.loss = log["loss"]
            self.progress_bar.set_postfix(step=f'{state.global_step}/{state.max_steps}', loss=self.loss)

    def on_evaluate(self, args: TrainingArguments, state: TrainerState, control: TrainerControl, logs=None, **kwargs):
        log = state.log_history[-1]
        print()
        print(f"{'-' * 10} Evaluation {'-' * 10}")
        print(pd.DataFrame.from_dict(log, orient="index").T.to_string(index=False))
        print(f"{'-' * 10} End Evaluation {'-' * 10}")
        print()

    def on_train_end(self, args: TrainingArguments, state: TrainerState, control: TrainerControl, **kwargs):
        self.progress_bar.close()


tokenizer = AutoTokenizer.from_pretrained(BASE_MODEL, add_prefix_space=True)
data_collator = DataCollatorForTokenClassification(tokenizer=tokenizer)


def preprocess(token_list: list[list[str]]):
    return [[token.replace("@USER", "@user").replace("HTTPURL", "url") for token in tokens] for tokens in token_list]


def tokenize_and_align_labels(examples, token_column, label_column):
    tokenized_inputs = tokenizer(preprocess(examples[token_column]), truncation=True, is_split_into_words=True)

    labels = []
    for i, label in enumerate(examples[label_column]):
        word_ids = tokenized_inputs.word_ids(batch_index=i)
        previous_word_idx = None
        label_ids = []

        for word_idx in word_ids:  # Set the special tokens to -100.
            if word_idx is None:
                label_ids.append(-100)
            elif word_idx != previous_word_idx:  # Only label the first token of a given word.
                label_ids.append(label[word_idx])
            else:
                label_ids.append(-100)
            previous_word_idx = word_idx
        labels.append(label_ids)
    tokenized_inputs["labels"] = labels

    return tokenized_inputs


metric = evaluate.load("seqeval")


def compute_metrics(p):
    predictions, labels = p

    predictions = np.argmax(predictions, axis=2)

    true_predictions = [
        [label_names[p] for (p, l) in zip(prediction, label) if l != -100]
        for prediction, label in zip(predictions, labels)
    ]

    true_labels = [
        [label_names[l] for (p, l) in zip(prediction, label) if l != -100]
        for prediction, label in zip(predictions, labels)
    ]

    results = metric.compute(predictions=true_predictions, references=true_labels)

    flattened_results = {
        "overall_precision": results["overall_precision"],
        "overall_recall": results["overall_recall"],
        "overall_f1": results["overall_f1"],
        "overall_accuracy": results["overall_accuracy"],
    }

    for k in results.keys():
        if k not in flattened_results.keys():
            flattened_results[k + "_f1"] = results[k]["f1"]

    return flattened_results


all_results = {}


dataset = load_from_disk(f"./data/train_test_all")

pbar = tqdm(total=10, desc="Training...")
for mode in ["IOB", "IOBES"]:
    pbar.set_postfix(mode=mode)
    for prep in ["none", "lemmatized", "stemmed", "uncased", "lemmatized_uncased"]:
        pbar.set_postfix(mode=mode, prep=prep)

        token_column_name = f"tokens_{prep}"
        label_column_name = f"ner_tags_{mode}"

        label_names = dataset["train"].features[label_column_name].feature.names

        label_to_id = {label: i for i, label in enumerate(label_names)}
        id_to_label = {i: label for i, label in enumerate(label_names)}

        train_dataset = dataset["train"]
        test_dataset = dataset["test"]

        tokenized_train = train_dataset.map(
            lambda x: tokenize_and_align_labels(x, token_column_name, label_column_name),
            batched=True, remove_columns=train_dataset.column_names
        )
        tokenized_test = test_dataset.map(
            lambda x: tokenize_and_align_labels(x, token_column_name, label_column_name),
            batched=True, remove_columns=test_dataset.column_names
        )

        average_results = {}
        model_name = f"robertweet_{prep}_16_5e-5_{mode}"
        model_dir = f"{PATH_TO_DIR}/results/{model_name}"

        all_results[model_name] = {
            "learning_rate": 5e-5,
            "batch_size": 16,
            "mode": mode,
            "prep": prep,
            "train_results": {}
        }

        model = AutoModelForTokenClassification.from_pretrained(BASE_MODEL, num_labels=len(label_names))

        training_args = TrainingArguments(
            output_dir=model_dir,
            overwrite_output_dir=True,
            num_train_epochs=5,
            per_device_train_batch_size=16,
            learning_rate=5e-5,
            warmup_steps=round(0.5 * len(tokenized_train) // 8),
            adam_beta1=0.9,
            adam_beta2=0.99,
            per_device_eval_batch_size=8,
            logging_steps=round(0.1 * len(tokenized_train) // 8),
            evaluation_strategy="epoch",
            save_strategy="no",
            report_to=["none"],
            seed=42
        )

        trainer = Trainer(
            model=model,
            args=training_args,
            train_dataset=tokenized_train,
            eval_dataset=tokenized_test,
            data_collator=data_collator,
            tokenizer=tokenizer,
            compute_metrics=compute_metrics
        )

        trainer.remove_callback(ProgressCallback)
        trainer.remove_callback(PrinterCallback)
        trainer.add_callback(TQDMCallback())

        trainer.train()
        pbar.update(1)

        try:
            all_results[model_name]["train_results"] = trainer.state.log_history

            # save all results
            with open(f"{PATH_TO_DIR}/results/label_results_new_base.json", 'w') as f:
                json.dump(all_results, f)
        # catch error if results are not able to be saved
        except TypeError as e:
            print(f"Could not save results for {model_name} due to {e}")
        except KeyError as e:
            print(f"Could not save model {model_name} due to {e}")
        except IOError as e:
            print(f"Could not save model {model_name} due to {e}")

        # delete model to save space
        del model, trainer, training_args

        # clear cache to save space
        torch.cuda.empty_cache()

        exit(0)
