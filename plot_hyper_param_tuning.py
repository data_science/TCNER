import json

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# read the results from ./results/all_results_2.json
with open("./results/all_results.json", "r") as f:
    all_results = json.load(f)


collect_metrics_for = ["eval_overall_f1", "eval_loss"]
avg_test_results = {}
for model_name, model_results in all_results.items():
    if int(model_name.split("_")[1]) > 16:
        continue
    avg_test_results[model_name] = {}
    for fold in model_results["train_results"]:
        temp = {}
        for entry in model_results["train_results"][fold]:
            if "eval_overall_f1" in entry:
                for metric in collect_metrics_for:
                    if metric not in temp:
                        temp[metric] = []
                    temp[metric].append(entry[metric])
        # take the max of each metric
        for metric in collect_metrics_for:
            if metric not in avg_test_results[model_name]:
                avg_test_results[model_name][metric] = []
            avg_test_results[model_name][metric].append(max(temp[metric]))
    avg_test_results[model_name]["learning_rate"] = model_results["learning_rate"]
    avg_test_results[model_name]["batch_size"] = model_results["batch_size"]
    # calculate the average
    for metric in set(avg_test_results[model_name]):
        if metric not in ["learning_rate", "batch_size"]:
            temp = {
                "avg": np.mean(avg_test_results[model_name][metric]),
                "std": np.std(avg_test_results[model_name][metric]),
                "min": round(np.min(avg_test_results[model_name][metric]), 6),
                "max": round(np.max(avg_test_results[model_name][metric]), 6)
            }
            for key, value in temp.items():
                avg_test_results[model_name][f"{metric}_{key}"] = value
            del avg_test_results[model_name][metric]

# pprint(avg_test_results)

# make avg_test_results into a nested dictionary with keys being the hyperparameters and values being the results
# example:
# {
#     "learning_rate": {
#         "5e-05": {
#             "batch_size": {
#                 "8": {
#                     "weight_decay": {
#                         "0.001": {
#                             "overall_f1": 0.7801234166937316,
#                          },
#                     },
#                 },
#             },
#         },
#     },
# }

# make a dataframe a single dataframe
df = pd.DataFrame(avg_test_results).T
# df = df.drop(columns=["epoch", "eval_runtime", "eval_samples_per_second", "eval_steps_per_second", "step", "eval_loss"])
iterables = [df["batch_size"].unique(), df["learning_rate"].unique()]
index = pd.MultiIndex.from_product(iterables, names=["batch_size", "learning_rate"])
df = df.set_index(["batch_size", "learning_rate"]).reindex(index)
# df = df.rename(
#     columns={
#         "eval_overall_f1_avg": "F1",
#         "eval_overall_precision_avg": "Precision",
#         "eval_overall_recall_avg": "Recall",
#         "eval_loss_avg": "Loss",
#     }
# )
df = df.rename(
    columns={
        column: column
        .replace("eval_overall_f1_", "F1 ")
        .replace("eval_overall_precision_", "Precision ")
        .replace("eval_overall_recall_", "Recall ")
        .replace("eval_loss_", "Loss ")
        for column in df.columns
    }
)
# keep only the column: mean_f1, std_f1
print(df.to_string())
# convert the dataframe to latex format and save it to a file
with open("./results/hyper_param_results.tex", "w") as f:
    f.write(df.to_latex())
# sort on overall_f1
# df = df.sort_values(by="eval_overall_f1", ascending=False)
# print(df)
exit(0)
# take the average of the train_results for the loss
avg_train_loss = {}
for model_name, model_results in all_results.items():
    avg_train_loss[model_name] = {
        'x': [],
        'y': [],
    }
    for fold, entries in model_results["train_results"].items():
        # get the loss for the entire fold
        avg_train_loss[model_name]['y'].append(np.array([entry["loss"] for entry in entries if "loss" in entry]))
        avg_train_loss[model_name]['x'].append(
            np.array([entry["epoch"] for entry in entries[:len(avg_train_loss[model_name]['y'][-1])]]))

    # calculate the average column-wise

    avg_train_loss[model_name]["x"] = np.mean(np.array(avg_train_loss[model_name]['x']), axis=0)
    avg_train_loss[model_name]["y"] = np.mean(np.array(avg_train_loss[model_name]['y']), axis=0)

# pprint(avg_train_loss)

# plot the loss during the training for each model
# label the plots with the hyperparameters used
plt.figure(figsize=(5, 5))
for model_name, model_results in avg_train_loss.items():
    lr, bs, wd = all_results[model_name]["learning_rate"], all_results[model_name]["batch_size"], \
        all_results[model_name]["weight_decay"]
    if bs not in [8, 16]:
        continue
    plt.plot(model_results["x"], model_results["y"], label=f"lr: {lr}, bs: {bs}")

plt.xlabel("Epoch")
plt.ylabel("Loss")
plt.legend()
plt.tight_layout()
plt.show()
