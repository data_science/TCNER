import json

import torch
import numpy as np
from sklearn.model_selection import KFold

import evaluate
from datasets import load_from_disk
from transformers import TrainingArguments, Trainer
from transformers import DataCollatorForTokenClassification
from transformers import AutoTokenizer, AutoModelForTokenClassification

BASE_MODEL = "cardiffnlp/twitter-roberta-base"

PATH_TO_DIR = "."


tokenizer = AutoTokenizer.from_pretrained(BASE_MODEL, add_prefix_space=True)
data_collator = DataCollatorForTokenClassification(tokenizer=tokenizer)


def preprocess(tokens: list[str]):
    return [token.replace("@USER", "@user").replace("HTTPURL", "url") for token in tokens]


def tokenize_and_align_labels(examples):
    tokenized_inputs = tokenizer(preprocess(examples["tweet"]), truncation=True, is_split_into_words=True)
    labels = []

    for i, label in enumerate(examples[f"ner_tags"]):

        word_ids = tokenized_inputs.word_ids(batch_index=i)  # Map tokens to their respective word.
        previous_word_idx = None
        label_ids = []

        for word_idx in word_ids:  # Set the special tokens to -100.
            if word_idx is None:
                label_ids.append(-100)
            elif word_idx != previous_word_idx:  # Only label the first token of a given word.
                label_ids.append(label[word_idx])
            else:
                label_ids.append(-100)
            previous_word_idx = word_idx
        labels.append(label_ids)
    tokenized_inputs["labels"] = labels

    return tokenized_inputs


metric = evaluate.load("seqeval")


def compute_metrics(p):
    predictions, labels = p

    predictions = np.argmax(predictions, axis=2)

    true_predictions = [
        [label_names[p] for (p, l) in zip(prediction, label) if l != -100]
        for prediction, label in zip(predictions, labels)
    ]

    true_labels = [
        [label_names[l] for (p, l) in zip(prediction, label) if l != -100]
        for prediction, label in zip(predictions, labels)
    ]

    results = metric.compute(predictions=true_predictions, references=true_labels)

    flattened_results = {
        "overall_precision": results["overall_precision"],
        "overall_recall": results["overall_recall"],
        "overall_f1": results["overall_f1"],
        "overall_accuracy": results["overall_accuracy"],
    }

    for k in results.keys():
        if k not in flattened_results.keys():
            flattened_results[k + "_f1"] = results[k]["f1"]

    return flattened_results


all_results = {}
do_kfold = False

for mode in ["IOB", "IOBES"]:
    dataset = load_from_disk(f"./data/old/train_test_{mode}_none")
    label_names = dataset["train"].features[f"ner_tags"].feature.names

    label_to_id = {label: i for i, label in enumerate(label_names)}
    id_to_label = {i: label for i, label in enumerate(label_names)}

    train_dataset = dataset["train"]
    test_dataset = dataset["test"]

    tokenized_train = train_dataset.map(
        tokenize_and_align_labels, batched=True, batch_size=128, remove_columns=train_dataset.column_names
    )
    tokenized_test = test_dataset.map(
        tokenize_and_align_labels, batched=True, batch_size=128, remove_columns=test_dataset.column_names
    )

    for batch_size in [8, 16]:
        for lr in [5e-5, 1e-4, 5e-4]:
            average_results = {}
            model_name = f"robertweet_none_{batch_size}_{str(lr).replace('.', '_')}_{mode}"
            model_dir = f"{PATH_TO_DIR}/results/{model_name}"

            all_results[model_name] = {
                "learning_rate": lr,
                "batch_size": batch_size,
                "train_results": {},
                "test_results": {}
            }

            kf = KFold(n_splits=5, shuffle=True, random_state=42)
            for fold, (train_index, test_index) in enumerate(kf.split(tokenized_train)):

                model = AutoModelForTokenClassification.from_pretrained(BASE_MODEL, num_labels=len(label_names))

                training_args = TrainingArguments(
                    output_dir=model_dir,
                    overwrite_output_dir=True,
                    num_train_epochs=1,
                    per_device_train_batch_size=batch_size,
                    learning_rate=lr,
                    warmup_steps=round(0.5 * len(train_index) // batch_size),
                    adam_beta1=0.9,
                    adam_beta2=0.99,
                    per_device_eval_batch_size=8,
                    logging_steps=round(0.1 * len(train_index) // batch_size),
                    evaluation_strategy="epoch",
                    save_strategy="no",
                )

                trainer = Trainer(
                    model=model,
                    args=training_args,
                    train_dataset=tokenized_train.select(train_index),
                    eval_dataset=tokenized_train.select(test_index),
                    data_collator=data_collator,
                    tokenizer=tokenizer,
                    compute_metrics=compute_metrics
                )

                trainer.train()

                try:
                    all_results[model_name]["train_results"][fold] = trainer.state.log_history
                    all_results[model_name]["test_results"][fold] = trainer.evaluate(tokenized_test)

                    # save all results
                    with open(f"{PATH_TO_DIR}/results/label_results.json", 'w') as f:
                        json.dump(all_results, f)
                # catch error if results are not able to be saved
                except TypeError as e:
                    print(f"Could not save results for {model_name} fold {fold} due to {e}")
                except KeyError as e:
                    print(f"Could not save model {model_name} for fold {fold} dur to {e}")
                except IOError as e:
                    print(f"Could not save model {model_name} for fold {fold} due to {e}")

                # delete model to save space
                del model, trainer, training_args

                # clear cache to save space
                torch.cuda.empty_cache()
