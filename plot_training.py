import os
import json
from pprint import pprint
import matplotlib.pyplot as plt

# read the results from ./results/label_results.json

with open("./results/label_results.json", "r") as f:
    all_results = json.load(f)

# iterate through the train_results for each model in all_results to find the epoch with the best eval_overall_f1
# and save the corresponding metrics in a new dictionary

best_results = {}

for model_name, model_results in all_results.items():
    best_results[model_name] = {}
    # iterate through the folds
    temp = []
    best_f1 = 0
    for entry in model_results["train_results"]:
        if "eval_overall_f1" in entry:
            epoch = entry
            if epoch["eval_overall_f1"] > best_f1:
                best_f1 = epoch["eval_overall_f1"]
                best_results[model_name] = epoch
        temp.append(best_f1)
    best_results[model_name]["best_f1"] = max(temp)
    best_results[model_name]["learning_rate"] = model_results["learning_rate"]
    best_results[model_name]["batch_size"] = model_results["batch_size"]
    best_results[model_name]["prep"] = model_name.split("_")[1]
    best_results[model_name]["mode"] = model_name.split("_")[-1]

# create a dataframe from the best_results dictionary
import pandas as pd
iterables = [["roberta-base", "roberta-tweet"], ["IOB", "IOBES"], ["none", "lemmatized", "stemmed", "uncased", "lemmatized_uncased"]]
index = pd.MultiIndex.from_product(iterables, names=["model", "mode", "prep"])
df = pd.DataFrame.from_dict(best_results, orient="index").set_index(index).drop(["eval_runtime", "eval_samples_per_second", "eval_steps_per_second", "step", "learning_rate", "batch_size", "prep", "mode"], axis=1)
df.drop(["eval_overall_precision", "eval_overall_recall", "eval_overall_accuracy", "best_f1"], axis=1, inplace=True)
df.rename(columns={
    column: column.replace("eval_", "").replace("_", " ").replace("f1", "F1") for column in df.columns
}, inplace=True)
print(df.to_string())

# convert df to latex table and save it to ./results/label_results.tex
# with open("./results/label_results.tex", "w") as f:
#     f.write(df.to_latex(index=True))
