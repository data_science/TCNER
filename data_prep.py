import re

import pandas as pd
from nltk.tokenize import TweetTokenizer
from datasets import Dataset, DatasetDict, Features, Value, ClassLabel, Sequence

TKNZR = TweetTokenizer()

PATTERN = re.compile(r'[,."!?;:\\\/\(\)\[\]\{\}<>|$#@%^\*~`\+\-=\t\n\r\x0b\x0c]', re.UNICODE)
SPACE = ' '

SINGLE = 'S'
END = 'E'
BEGINNING = 'B'
INSIDE = 'I'
OUTSIDE = 'O'
NER_SEP = '-'

NE_SEP = r'/'
MAX_SPLIT = 1
TAG_SEP = r';'

PRE_MENTION = '@FakeUsername'
MID_MENTION = '_Mention_'
MENTION = '@USER'

PRE_HASHTAG = '#FakeHashtag'
MID_HASHTAG = '_HASHTAG_'
HASHTAG = ''

PRE_URL = 'http://FakeURL'
MID_URL = '_URL_'
URL = 'HTTPURL'


def normalize_tweet(tweet):
    norm_tweet = tweet.replace("…", "...")
    norm_tweet = norm_tweet.replace("’", "'")
    norm_tweet = norm_tweet.replace("`", "'")
    norm_tweet = norm_tweet.replace("‘", "'")
    norm_tweet = norm_tweet.replace("´", "'")
    norm_tweet = norm_tweet.replace("“", '"')

    norm_tweet = (
        norm_tweet.replace("cannot ", "can not ")
        .replace("n't ", " n't ")
        .replace("n 't ", " n't ")
        .replace("ca n't", "can't")
        .replace("ai n't", "ain't")
    )

    # if there is an apostrophe in between the word, then add a space before the apostrophe
    norm_tweet = re.sub(r"(\w)(\'\w)", r"\1 \2", norm_tweet)
    norm_tweet = (
        norm_tweet.replace(" p . m .", "  p.m.")
        .replace(" p . m ", " p.m ")
        .replace(" a . m .", " a.m.")
        .replace(" a . m ", " a.m ")
    )
    norm_tweet = re.sub(r"\'(?!\w)", SPACE, norm_tweet)  # remove apostrophe that is not followed by a word character
    norm_tweet = PATTERN.sub(SPACE, norm_tweet)  # replace punctuation with space
    norm_tweet = re.sub(r"(\d)\s+(\d)", r"\1\2", norm_tweet)  # remove space between digits

    return " ".join(norm_tweet.split())


iob_list = ['O', 'B-ORG', 'I-ORG', 'B-LOC', 'I-LOC', 'B-PER', 'I-PER', 'B-MISC', 'I-MISC']
features_iob = Features(
    {
        'tweet': Sequence(Value(dtype='string')),
        'ner_tags': Sequence(ClassLabel(num_classes=9, names=iob_list, id=None)),
    }
)

iobes_list = [
    'O',
    'B-ORG', 'I-ORG', 'E-ORG', 'S-ORG', 'B-LOC', 'I-LOC', 'E-LOC', 'S-LOC',
    'B-PER', 'I-PER', 'E-PER', 'S-PER', 'B-MISC', 'I-MISC', 'E-MISC', 'S-MISC',
]
features_iobes = Features(
    {
        'tweet': Sequence(Value(dtype='string')),
        'ner_tags': Sequence(ClassLabel(num_classes=17, names=iobes_list, id=None)),
    }
)


def assign_ner_tags(corpora, method='IOB'):
    # temporary list to store the tweets and the ner tags
    ner_tagged = []

    prefix_begin = f'{BEGINNING}{NER_SEP}'
    prefix_inside = f'{INSIDE}{NER_SEP}'

    if method == 'IOB':
        prefix_single = f'{BEGINNING}{NER_SEP}'
        prefix_end = f'{INSIDE}{NER_SEP}'
        class_list = iob_list
    else:
        prefix_single = f'{SINGLE}{NER_SEP}'
        prefix_end = f'{END}{NER_SEP}'
        class_list = iobes_list

    for line in corpora:
        _, tags, tweet = line.strip().split('\t')

        # preprocess the line by replacing the mentions, hashtags and urls with special tokens
        tweet = tweet.replace(PRE_MENTION, MID_MENTION)
        tweet = tweet.replace(PRE_HASHTAG, MID_HASHTAG)
        tweet = tweet.replace(PRE_URL, MID_URL)

        # normalize the tweet
        tweet = normalize_tweet(tweet)

        tweet = tweet.replace(MID_MENTION, MENTION)
        tweet = tweet.replace(MID_HASHTAG, HASHTAG)
        tweet = tweet.replace(MID_URL, URL)

        split_tags = list(
            map(
                lambda tag_pair: [
                    (name_pair := tag_pair.split(NE_SEP, MAX_SPLIT))[0].strip().upper(),
                    TKNZR.tokenize(normalize_tweet(name_pair[1].strip()))
                ],
                filter(lambda tag: tag is not None and tag != '', tags.split(TAG_SEP))
            )
        )

        # split the tweet into tokens
        tweet = TKNZR.tokenize(tweet)

        ner_tags = [0 for _ in range(len(tweet))]
        if split_tags:

            for (name, entity) in split_tags:
                found = False
                counter = 0

                for word in tweet:
                    if word == entity[counter]:
                        counter += 1
                        counter, found = check_if_entity(counter, entity, found, name, ner_tags, prefix_begin,
                                                         prefix_end, prefix_inside, prefix_single, tweet, word,
                                                         class_list)
                    else:
                        counter = 0
                        if word == entity[counter]:
                            counter += 1
                            counter, found = check_if_entity(counter, entity, found, name, ner_tags, prefix_begin,
                                                             prefix_end, prefix_inside, prefix_single, tweet, word,
                                                             class_list)
                if not found:
                    print(tags)
                    print(split_tags)
                    print(f'Could not find {entity} in {tweet} \n (string: {" ".join(tweet)})')
                    exit(1)

        # add the tweet and ner tags to the temporary list
        ner_tagged.append((tweet, ner_tags))

    # convert the temporary list to a pandas dataframe
    ner_tagged = pd.DataFrame(ner_tagged, columns=['tweet', 'ner_tags'])
    print(ner_tagged.head())

    return ner_tagged


def check_if_entity(counter, entity, found, name, ner_tags, prefix_begin, prefix_end, prefix_inside, prefix_single,
                    tweet, word, class_list):
    if counter == len(entity):
        if len(entity) == 1:
            ner_tags[tweet.index(word)] = class_list.index(f'{prefix_single}{name}')
        else:
            ner_tags[tweet.index(word) - len(entity) + 1] = class_list.index(f'{prefix_begin}{name}')
            for i in range(tweet.index(word) - len(entity) + 2, tweet.index(word) + 1):
                ner_tags[i] = class_list.index(f'{prefix_inside}{name}')
            ner_tags[tweet.index(word)] = class_list.index(f'{prefix_end}{name}')
        counter = 0
        found = True
    return counter, found


if __name__ == '__main__':

    for mode in ['IOB', 'IOBES']:
        dataset = []
        for file in ['./data/orig/TweetsTrainset.txt', './data/orig/TweetsTestGroundTruth.txt']:
            with open(file, 'r') as f:
                dataset.append(assign_ner_tags(f, mode))

        if mode == 'IOB':
            features = features_iob
        else:
            features = features_iobes

        train, test = Dataset.from_pandas(dataset[0], features), Dataset.from_pandas(dataset[1], features)

        # create dataset dictionary with the train and test datasets and the labels with type list of strings
        ds = DatasetDict({'train': train, 'test': test})
        ds.save_to_disk(f'./data/train_test_{mode}')
        print(f'Finished saving {mode} dataset')
